import Vue from 'vue'
import Router from 'vue-router'
import moduleRouter from './fileLoad'
import routerData from './mould'

import vueResource from 'vue-resource'

Vue.use(Router)
Vue.use(vueResource)

export default new Router({
  base: '/',
  routes: [{
    path: '/',
    name: routerData.index.name,
    component: moduleRouter.index
  }, {
    path: routerData.index.path,
    redirect: '/'
  }, {
    path: routerData.notFind.path,
    name: routerData.notFind.name,
    component: moduleRouter.notFind
  }]
})
