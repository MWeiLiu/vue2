/*
 * @Author: MWeiLiu
 * @Date: 2019-12-18 12:03:54
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 12:45:56
 */

const express = require('express')
const app = express()

let router = express.Router()

module.exports = {
  app,
  router
}
