/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:11:14
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 13:48:42
 */
import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user
  }
})
