# mweiliu_web_cli

> mweiliu_web_cli

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## version 1.1.0
### node 12.12.1
### npm 6.12.1
### cli 2

1. 内置vue-router
2. 内置vuex
3. 内置axios
4. 内置element-ui
5. 内置express本地模拟服务器