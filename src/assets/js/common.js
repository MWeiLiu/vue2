/* eslint-disable space-before-function-paren */
/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:16:27
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2021-04-26 20:50:39
 */

// import Conf from './config'
import Md5 from './md5'
import Base64 from './base64'
// const service = require('../../assets/js/request')
const env = process.env

class Common {
  /** 路由管理
   *
   * @static
   * @param {*} option
   * @returns
   * @memberof Common
   */
  static routeManagement(option) {
    var conf = {
      code: 1,
      msg: ''
    }

    let userinfo = localStorage.getItem('userinfo')
    if (!userinfo || userinfo === '') {
      // 是否存在
      if (option.to.name && (this.inArray(option.to.name, option.guard.need) > -1 ||
          this.inArray(option.to.name, option.guard.unwante) > -1 ||
          this.inArray(option.to.name, option.guard.public) > -1)) {
        // 没有登录
        if (this.inArray(option.to.name, option.guard.need) > -1) {
          conf.msg = '请先登录'
          conf.code = -1
          alert(conf.msg)
        }
      } else {
        conf.code = 404
        conf.msg = '您访问的页面不存在'
      }
    } else {
      // 是否存在
      if (option.to.name && (this.inArray(option.to.name, option.guard.need) > -1 ||
          this.inArray(option.to.name, option.guard.unwante) > -1 ||
          this.inArray(option.to.name, option.guard.public) > -1)) {
        // 已登录
        if (this.inArray(option.to.name, option.guard.unwante) > -1) {
          conf.code = 0
        } else {
          conf.code = 1
          conf.msg = '欢迎回来'
        }
      } else {
        conf.code = 404
        conf.msg = '您访问的页面不存在'
      }
    }
    return conf
  }

  /**
   * 查询数组中的字符
   *
   * @static
   * @param {*} str
   * @param {*} arr
   * @returns
   * @memberof Common
   */
  static inArray(str, arr) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === str) {
        return i
      }
    }
    return -1
  }

  /**
   * 获取参数
   *
   * @static
   * @returns
   * @memberof Common
   */
  static getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')
    var r = window.location.search.substr(1).match(reg)
    if (r != null) {
      return decodeURI(r[2])
    }
    return null
  }

  /**
   * RequestAnimationFrame
   *
   * @static
   * @memberof Common
   */
  static myRequestAnimationFrame() {
    var lastTime = 0
    var vendors = ['webkit', 'moz']
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame']
      window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] ||
        window[vendors[x] + 'CancelRequestAnimationFrame']
    }

    if (!window.requestAnimationFrame) {
      window.requestAnimationFrame = function (myCallback, element) {
        var currTime = new Date().getTime()
        var timeToCall = Math.max(0, 16.7 - (currTime - lastTime))
        var id = window.setTimeout(myCallback => {
          myCallback(currTime + timeToCall)
        }, timeToCall)
        lastTime = currTime + timeToCall
        return id
      }
    }
    if (!window.cancelAnimationFrame) {
      window.cancelAnimationFrame = function (id) {
        clearTimeout(id)
      }
    }
  }
  /**
   * 解码
   *
   * @param {*} base64
   * @returns
   * @memberof Common
   */
  static decode(base64) {
    if (typeof base64 === 'string') {
      let baseArr = base64.split('|')

      let str = (new Base64()).base64_decode(baseArr[1])
      let data = str.substring(0, str.length - 32)
      let md5 = str.substring(data.length)

      let newMd5 = Md5(data + env.TOKEN)

      if (newMd5 === md5) {
        return JSON.parse(data) || {}
      } else {
        return {}
      }
    } else {
      return base64
    }
  }

  /**
   * 编码
   *
   * @param {*} info
   * @memberof Common
   */
  static encode(info) {
    let A = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_@$*'
    let begin35 = 35
    let begini = 0
    let begin = ''
    let end18 = 18
    let endi = 0
    let end = ''

    while (begini < begin35) {
      let r = parseInt(Math.random() * A.length)
      begin += A[r]
      begini++
    }

    while (endi < end18) {
      let r = parseInt(Math.random() * A.length)
      end += A[r]
      endi++
    }

    info = `${begin}|${(new Base64()).base64_encode(info + Md5(info + env.TOKEN).toLowerCase())}|${end}`

    return info
  }

  static setCookie(option) {
    document.cookie = `${option.key}=${option.value}`
  }

  static getCookie(key) {
    let strCookie = document.cookie
    let arrCookie = strCookie.split(';')
    let value = ''
    for (var i = 0; i < arrCookie.length; i++) {
      let arr = arrCookie[i].split('=')
      if (key === arr[0]) {
        value = arr[1]
        break
      }
    }
    return value
  }

  static hasCookie(key) {
    let strCookie = document.cookie
    let arrCookie = strCookie.split(';')
    let value
    for (var i = 0; i < arrCookie.length; i++) {
      let arr = arrCookie[i].split('=')
      if (key === arr[0]) {
        value = arr[1]
        break
      }
    }
    if (value) {
      return true
    } else {
      return false
    }
  }
}

export default Common
