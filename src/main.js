// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import jquery from 'jquery'

import router from '@/router'
import mouldRouter from '@/router/mould'
import store from '@/store'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

const env = process.env
console.log(env)

Vue.config.productionTip = env.DEBUG
Vue.config.silent = !env.DEBUG
Vue.config.devtools = env.DEBUG

if (env.DEBUG) {
  Vue.config.warnHandler = function (err, vm, info) {
    console.log(err)
    console.log(vm)
    console.log(info)
  }
}

Vue.prototype.$Router = mouldRouter
Vue.prototype.$_ = jquery

Vue.use(ElementUI)

window._vue = Vue

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
