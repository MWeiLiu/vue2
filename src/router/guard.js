/* eslint-disable comma-dangle */
/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:13:17
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 14:26:00
 */

import mould from './mould'

const guard = {

  /**
   * 需要
   */
  need: [],

  /**
   * 不需要
   */
  unwante: [
    mould.notFind.name,
    mould.index.name,
  ],
  /**
   * 公共
   */
  public: []

}

export default guard
