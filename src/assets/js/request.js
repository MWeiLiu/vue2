/* eslint-disable space-before-function-paren */
/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:19:17
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2021-04-26 22:39:03
 */

import Common from './common'
const axios = require('axios')
const QS = require('QS')
const api = require('./api.js')
const env = process.env

// 设置超时时间
axios.defaults.timeout = 10000
// post请求头
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

function get(url, params) {
  return new Promise((resolve, reject) => {
    axios.get(url, params).then(res => {
      resolve(res.data)
    }).catch(err => {
      reject(err)
    })
  })
}

function post(url, params) {
  return new Promise((resolve, reject) => {
    axios.post(url, QS.stringify(params)).then(res => {
      resolve(res.data)
    }).catch(err => {
      reject(err)
    })
  })
}

function upfilepost(that, url, params) {
  let dataForm = new FormData()

  for (let i in params) {
    dataForm.append(i, params[i])
  }

  return new Promise((resolve, reject) => {
    that.$_.ajax({
      url: url,
      method: 'POST',
      data: dataForm,
      processData: false,
      contentType: false,
      dataType: 'json',
      success: function (res) {
        resolve(res.data)
      },
      error: function (err) {
        reject(err)
      }
    })
  })
}

// 请求拦截器
axios.interceptors.request.use(
  confirm => {
    confirm.data = 'data=' + Common.encode(confirm.data)
    return confirm
  },
  error => {
    return Promise.error(error)
  }
)

// 响应拦截器
axios.interceptors.response.use(
  response => {
    if (response.status === 200) {
      if (!env.DEBUG && typeof response.data.data === 'string') {
        let data = Common.decode(response.data.data)
        response.data.data = data
      }
      return Promise.resolve(response)
    } else {
      return Promise.reject(response)
    }
  },
  error => {
    return Promise.reject(error.response)
  }
)

export function request({
  method,
  url,
  params = {}
}) {
  if (method === 'GET') {
    return get(`${env.REQUEST_URL}${api[url]}`, params)
  } else if (method === 'POST') {
    return post(`${env.REQUEST_URL}${api[url]}`, params)
  }
}

export function upfile({
  that,
  url,
  params
}) {
  return upfilepost(that, `${env.REQUEST_URL}${api[url]}`, params)
}

export function error(err, option) {
  console.log(err)
  option.that.$notify.error({
    title: '错误',
    message: '网络不给力，请重试！'
  })
}
