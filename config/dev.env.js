'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
const conf = require('./index')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  DEBUG: true,
  REQUEST_URL: `"http://localhost:${conf.dev.port}/"`,
  TOKEN:'"ShangGuChuangJing"'
})
