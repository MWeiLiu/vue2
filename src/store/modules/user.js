/* eslint-disable space-before-function-paren */
/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:49:24
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 14:30:11
 */

const state = {
  userInfo: {}
}

const actions = {
  set_userInfo(context, option) {
    context.commit('set_userInfo', option)
  }
}

const getters = {
  getAge: state => {
    return state.userInfo.age ? `${state.userInfo.age}岁` : ''
  },
  getSex: state => {
    return state.userInfo.sex ? (state.userInfo.sex === 'man' ? '男' : '女') : ''
  }
}

const mutations = {
  set_userInfo(state, option) {
    state.userInfo = option
  }
}

export default {
  state,
  actions,
  getters,
  mutations
}
