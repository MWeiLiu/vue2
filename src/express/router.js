/*
 * @Author: MWeiLiu
 * @Date: 2019-12-18 12:06:49
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 13:38:32
 */
const appData = require('./data')

const userInfo = (req, res) => {
  res.json({
    code: 1,
    data: appData.user,
    msg: ''
  })
}

module.exports = {
  userInfo
}
