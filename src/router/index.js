/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:15:37
 * @Last Modified by:   MWeiLiu
 * @Last Modified time: 2019-12-17 20:15:37
 */

import router from './router'
import mould from './mould'
import guard from './guard'
import Common from '../assets/js/common'

router.beforeEach((to, from, next) => {
  var routeManagement = Common.routeManagement({
    to: to,
    guard: guard,
    store: router.app.$options.store,
    router: router
  })

  if (routeManagement.code === -1) {
    router.push(mould.login)
    return
  } else if (routeManagement.code === 0) {
    router.push(mould.index)
    return
  } else if (routeManagement.code === 404) {
    router.push(mould.notFind)
    return
  }

  next()
})

router.afterEach((to, from, next) => {
  window.scrollTo(0, 0)
})

export default router
