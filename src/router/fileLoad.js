/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:12:04
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 14:27:28
 */

// 404
const notFind = resolve => {
  require.ensure([], () => {
    resolve(require('@/views/notFind/notFind.vue'))
  }, 'notFind')
}

// 首页
const index = resolve => {
  require.ensure([], () => {
    resolve(require('@/views/index/index.vue'))
  }, 'index')
}

export default {
  notFind,
  index
}
