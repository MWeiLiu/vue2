/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:18:27
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2021-04-26 19:02:33
 */

const env = process.env

export default {
  env
}
