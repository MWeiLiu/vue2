/*
 * @Author: MWeiLiu
 * @Date: 2019-12-17 20:14:00
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 14:19:32
 */

const notFind = {
  path: '/404',
  name: 'notFind',
  value: '404'
}

const index = {
  path: '/index',
  name: 'index',
  value: '首页'
}

export default {
  notFind,
  index
}
