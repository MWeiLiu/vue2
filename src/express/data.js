/*
 * @Author: MWeiLiu
 * @Date: 2019-12-18 12:38:43
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2019-12-18 12:44:27
 */

const user = require('./data/user')

module.exports = {
  user
}
