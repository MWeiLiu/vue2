/*
 * @Author: MWeiLiu
 * @Date: 2019-12-18 12:30:10
 * @Last Modified by: MWeiLiu
 * @Last Modified time: 2021-04-26 20:11:40
 */

export const userInfo = 'api/userInfo'
